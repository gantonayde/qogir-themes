<div align="center">
<h1 align="center">
  <img src="https://cdn.pling.com/cache/85x85-crop/img/9/a/2/e/265b67dcdd6ac800e39233de0daf58a33a37.jpg" alt="Project">
  <br />
  Qogir Themes for Snaps
</h1>
</div>

<p align="center"><b>These are the Qogir GTK and icon theme for Snaps</b>. Qogir is a flat Design theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Unity, Budgie, Cinnamon Pantheon, XFCE, Mate, etc.


This snap contains all flavours of both the GTK and icon theme.

<b>Attributions</b>:
This snap is packaged from vinceliuice's <a href="https://github.com/vinceliuice/Qogir-theme">Qogir-theme</a> and <a href="https://github.com/vinceliuice/Qogir-icon-theme">Qogir-icon-theme</a>. The Qogir-theme and Qogir-icon-theme are under a GPL3.0 licence.

</p>


## Install <a href="https://snapcraft.io/qogir-themes">
<a href="https://snapcraft.io/qogir-themes">
<img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

You can install the snap from the Snap Store оr by running:
```
sudo snap install qogir-themes
```
To connect the theme to an app run:
```
sudo snap connect [other snap]:gtk-3-themes qogir-themes:gtk-3-themes 
```
```
sudo snap connect [other snap]:icon-themes qogir-themes:icon-themes
```

